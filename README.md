# Skia renderer for egui

This is a renderer for [egui](https://github.com/emilk/egui) that uses
[skia-safe](https://crates.io/crates/skia-safe).

This crate was extracted out of
[egui_skia](https://github.com/lucasmerlin/egui_skia) and does not contain its
winit integration.

## Usage

Have a look at the examples to get started.

```bash
cargo run --example color_test
cargo run --example rasterize
```
